#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 27 08:20:44 2020

@author: tash149
"""

#!/usr/bin/env python

from __future__ import print_function
import cv2
import sys
sys.path.insert(1, 'ar_markers/')
from ar_markers.detect import detect_markers


if __name__ == '__main__':
        capture = cv2.VideoCapture('Tag2.mp4')
        if capture.isOpened():  
                frame_captured, frame = capture.read()
        else:
                frame_captured = False

        while frame_captured:
                markers = detect_markers(frame)
                for marker in markers:
                        marker.highlite_marker(frame)
                cv2.imshow('Test Frame', frame)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                        break
                frame_captured, frame = capture.read()
        capture.release()
        cv2.destroyAllWindows()